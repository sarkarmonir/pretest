<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_test_cat_id');
            $table->integer('subject_id')->unsigned();
            $table->integer('chapter_id')->unsigned();
            $table->string('model_question_title');
            $table->string('question_hint')->nullable();
            $table->text('previous_exam')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('QA_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_questions');
    }
}
