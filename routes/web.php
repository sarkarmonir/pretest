<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');


//User Related Route
Route::get('/user','UserController@index');
Route::post('/user-store','UserController@store');
Route::post('/user-update','UserController@update');
Route::post('/user-delete/{id}','UserController@destroy');

//Exam Category Related Route
Route::get('/exam-category','ExamCategoryController@index');
Route::post('/exam-category-store','ExamCategoryController@store');
Route::post('/exam-cat-update','ExamCategoryController@update');
Route::post('/exam-category-delete/{id}','ExamCategoryController@destroy');

//Subject Related Route
Route::get('/subject','SubjectController@index');
Route::post('/subject-store','SubjectController@store');
Route::post('/subject-update','SubjectController@update');
Route::post('/subject-delete/{id}','SubjectController@destroy');

//Chapter Related Route
Route::get('/chapter','ChapterController@index');
Route::post('/chapter-store','ChapterController@store');
Route::post('/chapter-update','ChapterController@update');
Route::post('/chapter-delete/{id}','ChapterController@destroy');

//Question Related Route
Route::get('/upload-qa','QuestionController@index');
Route::post('/question-store','QuestionController@store');
Route::post('/question-update','QuestionController@update');
Route::post('/question-delete/{id}','QuestionController@destroy');

//Model Question Related Route
Route::get('/model-test','ModelQuestionController@index');
Route::post('/model-question-store','ModelQuestionController@store');
Route::post('/model-question-update','ModelQuestionController@update');
Route::post('/model-question-delete/{id}','ModelQuestionController@destroy');

//Model Test Category Related Route
Route::get('/model-test-category','ModelTestCategoryController@index');
Route::post('/model-test-category-store','ModelTestCategoryController@store');
Route::post('/model-test-cat-update','ModelTestCategoryController@update');
Route::post('/model-test-category-delete/{id}','ModelTestCategoryController@destroy');

//ajxa related route
Route::get('/get-question-answer-by-id', 'QuestionController@get_answer_data');
Route::get('/get-model-question-answer-by-id', 'ModelQuestionController@get_model_answer_data');
Route::get('/get-chapter-data-by-id', 'ModelQuestionController@get_chapter_data');
Route::get('/get-subject-data-by-id', 'ChapterController@get_subject_data');