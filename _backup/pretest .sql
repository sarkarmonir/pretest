-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2019 at 02:27 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pretest`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `answer_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer_option` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct_answer` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`answer_id`, `question_id`, `answer_option`, `correct_answer`, `created_at`, `updated_at`) VALUES
(1, 1, 'ভারত', 0, '2019-04-11 18:00:00', '2019-04-11 18:00:00'),
(2, 1, 'পাকিস্তান', 0, '2019-04-11 18:00:00', '2019-04-11 18:00:00'),
(3, 1, 'বাংলাদেশ', 1, '2019-04-11 18:00:00', '2019-04-11 18:00:00'),
(4, 1, 'নেপাল', 0, '2019-04-11 18:00:00', '2019-04-11 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `chapter_id` int(10) UNSIGNED NOT NULL,
  `exam_cat_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `chapter_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chapter_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `qa_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`chapter_id`, `exam_cat_id`, `subject_id`, `chapter_name`, `chapter_description`, `created_by`, `qa_by`, `created_at`, `updated_at`) VALUES
(2, 2, 3, 'Second Chapter', 'Second Chapter', 0, 0, '2019-04-02 13:21:19', '2019-04-07 23:26:18'),
(3, 2, 2, 'Third Chapter', 'Log26+log223', 0, 0, '2019-04-07 12:42:19', '2019-04-12 00:24:07'),
(4, 1, 1, 'chapter no 4', 'Description is the pattern of narrative development that aims to make vivid a place, object, character, or group. Description is one of four rhetorical modes, along with exposition, argumentation, and narration. In practice it would be difficult to write literature that drew on just one of the four basic modes.', 0, 0, '2019-04-07 12:43:45', '2019-04-07 23:31:08');

-- --------------------------------------------------------

--
-- Table structure for table `exam_categories`
--

CREATE TABLE `exam_categories` (
  `exam_cat_id` int(10) UNSIGNED NOT NULL,
  `exam_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `qa_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_categories`
--

INSERT INTO `exam_categories` (`exam_cat_id`, `exam_cat_name`, `created_by`, `qa_by`, `created_at`, `updated_at`) VALUES
(1, 'BCS', 0, 0, '2019-04-02 09:53:21', '2019-04-02 12:28:18'),
(2, 'SSC', 0, 0, NULL, NULL),
(3, 'HSC', 0, 0, '2019-04-07 12:38:41', '2019-04-07 12:38:41'),
(4, 'Masters', 0, 0, '2019-04-07 12:38:49', '2019-04-07 12:38:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_02_152021_create_exam_categories_table', 2),
(4, '2019_04_02_152157_create_subjects_table', 2),
(5, '2019_04_02_152211_create_chapters_table', 2),
(6, '2019_04_02_154943_create_questions_table', 3),
(7, '2019_04_03_043948_create_questions_table', 4),
(8, '2019_04_03_044245_create_answers_table', 4),
(9, '2019_04_07_154945_create_model_questions_table', 5),
(10, '2019_04_07_155327_create_model_answers_table', 5),
(11, '2019_04_07_175640_create_model_test_categories_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `model_answers`
--

CREATE TABLE `model_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `m_question_id` int(10) UNSIGNED NOT NULL,
  `answer_option` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct_answer` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_answers`
--

INSERT INTO `model_answers` (`id`, `m_question_id`, `answer_option`, `correct_answer`, `created_at`, `updated_at`) VALUES
(1, 1, 'আত্মজীবনী', 0, '2019-04-07 18:00:00', NULL),
(2, 1, 'প্রানয় কাব্য', 0, '2019-04-07 18:00:00', NULL),
(3, 1, 'নীতি কাব্য', 1, '2019-04-07 18:00:00', NULL),
(4, 1, 'জঙ্গনামা', 0, '2019-04-07 18:00:00', NULL),
(5, 2, 'হরপ্রসাদ শাস্ত্রী', 0, '2019-04-07 18:00:00', NULL),
(6, 2, 'সকুমার সেন', 0, '2019-04-07 18:00:00', NULL),
(7, 2, 'মোহাম্মদ শহিদুল্লাহ', 0, '2019-04-07 18:00:00', NULL),
(8, 2, 'ড সুনীতিকুমার চট্টোপাধ্যায়', 1, '2019-04-07 18:00:00', NULL),
(9, 3, 'মনসা মঙ্গল', 0, '2019-04-07 18:00:00', NULL),
(10, 3, 'মনসা বিজয়', 1, '2019-04-07 18:00:00', NULL),
(11, 3, 'চাঁদ সওদাগর কাহিনী', 0, '2019-04-07 18:00:00', NULL),
(12, 3, 'মনসা প্রসাস্ত্রি', 0, '2019-04-07 18:00:00', NULL),
(13, 4, 'ঈশ্বরচন্দ্র গুপ্ত', 0, '2019-04-07 18:00:00', '2019-04-07 18:00:00'),
(14, 4, 'ভারত চন্দ্র রায়', 1, '2019-04-07 18:00:00', '2019-04-07 18:00:00'),
(15, 4, 'রাম রাম বসু', 0, '2019-04-07 18:00:00', '2019-04-07 18:00:00'),
(16, 4, 'শাহ মুহাম্মদ সাগীর', 0, '2019-04-07 18:00:00', '2019-04-07 18:00:00'),
(17, 5, 'ড দীনেশ চন্দ্র শেন', 0, '2019-04-07 18:00:00', NULL),
(18, 5, 'বাংলা একাডেমী', 0, '2019-04-07 18:00:00', NULL),
(19, 5, 'ঢাকা বিশ্ববিদ্যালয়', 0, '2019-04-07 18:00:00', NULL),
(20, 5, 'কলকাতা বিশ্ববিদ্যালয়', 1, '2019-04-07 18:00:00', NULL),
(21, 6, 'রামমোহন রায়', 0, '2019-04-07 18:00:00', NULL),
(22, 6, 'ঈশ্বরচন্দ্র বিদ্যাসাগর', 1, '2019-04-07 18:00:00', NULL),
(23, 6, 'বঙ্কিমচন্দ্র চট্টপাধ্যায়', 0, '2019-04-07 18:00:00', NULL),
(24, 6, 'মীর মশারফ হোসেন', 0, '2019-04-07 18:00:00', NULL),
(25, 7, 'বিষবৃক্ষ', 0, '2019-04-07 18:00:00', NULL),
(26, 7, 'সীতারাম', 0, '2019-04-07 18:00:00', NULL),
(27, 7, 'কপাল কুন্ডোলা', 0, '2019-04-07 18:00:00', NULL),
(28, 7, 'দুর্গেনন্দিনী', 1, '2019-04-07 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `model_questions`
--

CREATE TABLE `model_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_test_cat_id` int(11) NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `chapter_id` int(10) UNSIGNED NOT NULL,
  `model_question_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_hint` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_exam` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `QA_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_questions`
--

INSERT INTO `model_questions` (`id`, `model_test_cat_id`, `subject_id`, `chapter_id`, `model_question_title`, `question_hint`, `previous_exam`, `created_by`, `QA_by`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 0, 'আলাওলের \'তোহফা\' কোন ধরনের কাব্য ?', NULL, '', 0, 0, NULL, NULL),
(2, 2, 1, 0, 'চরযাপদ যে বাংলা ভাষাই রচিত এতি প্রথম কে প্রমান করেন ?', NULL, NULL, 0, 0, NULL, NULL),
(3, 2, 1, 0, 'বিপ্রদাস পিপিলাই রচিত কাব্যর নাম কি?', NULL, NULL, 0, 0, NULL, NULL),
(4, 2, 1, 4, 'বাংলা সাহিত্যর মধ্যযুগের (এবং মঙ্গল কাব্যর) শেষ কবি কে?', NULL, '/', 0, 0, NULL, NULL),
(5, 2, 1, 0, 'ময়মনসিংহগীতিকা এবং পূর্ব বঙ্গ গীতিকা প্রকাশ করেছে-', NULL, NULL, 0, 0, NULL, NULL),
(6, 2, 1, 0, 'বাংলা গদ্যের জনক কে?', NULL, NULL, 0, 0, NULL, NULL),
(7, 2, 1, 0, 'বঙ্কিমচন্দ্রের প্রথম বাংলা উপন্যাস কোনটি?', NULL, NULL, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `model_test_categories`
--

CREATE TABLE `model_test_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_test_cat_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `qa_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_test_categories`
--

INSERT INTO `model_test_categories` (`id`, `model_test_cat_title`, `created_by`, `qa_by`, `created_at`, `updated_at`) VALUES
(2, 'Model Test 1', 0, 0, '2019-04-07 12:22:37', '2019-04-08 06:49:33'),
(3, 'model test 3', 0, 0, '2019-04-07 12:22:45', '2019-04-07 12:22:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `chapter_id` int(10) UNSIGNED NOT NULL,
  `question_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_hint` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_exam` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `QA_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `subject_id`, `chapter_id`, `question_title`, `question_hint`, `previous_exam`, `created_by`, `QA_by`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'তোমার দেশের নাম কি?', NULL, 'বিসিএস ২০১৬', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(10) UNSIGNED NOT NULL,
  `exam_cat_id` int(10) UNSIGNED NOT NULL,
  `subject_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `qa_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `exam_cat_id`, `subject_name`, `created_by`, `qa_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Bangla', 0, 0, NULL, '2019-04-02 12:32:53'),
(2, 2, 'English', 0, 0, '2019-04-07 12:39:02', '2019-04-07 12:39:02'),
(3, 2, 'Math', 0, 0, '2019-04-07 12:39:13', '2019-04-07 12:39:13'),
(4, 3, 'ICT', 0, 0, '2019-04-07 12:39:21', '2019-04-07 12:39:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '0=super_admin, 1=question_uploader, 2=qa_tester, 3= general_user',
  `created_by` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `user_type`, `created_by`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'superadmin@pretest.com', NULL, '$2y$10$2CHeksR7tNrsGrSYouG9FuhMlxeYyD/RX.B/VU5EgAZpSEWSkD4FG', 0, 0, 1, 'NqacAcYSaAP4Ijxib6t5wvWkdic2hbhqkTEy3ik0iaHanGrYaskhWlz5aa9D', NULL, NULL),
(5, 'sabber hossian', 'sabber@gmail.com', NULL, '$2y$10$F5AFvS957IH.3B8273s8dOKC66zmTwNQ/8CooFphVxX2MvEeVRumm', 2, 1, 1, 'DKJq0v13K6fcDKrDF5j8XqO2TR47ZokjPP2bPMWiahxxkJkQHl5JUnLq3ENh', '2019-04-02 06:34:04', '2019-04-02 06:34:04'),
(7, 'test', 'test@test.com', NULL, '$2y$10$BJm.uWkTMo9dE.sVqOCU/uz8jn0sB4iWpwef/uxgMCf6xY5ggwV5S', 1, 6, 1, NULL, '2019-04-02 09:17:44', '2019-04-02 09:21:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`chapter_id`);

--
-- Indexes for table `exam_categories`
--
ALTER TABLE `exam_categories`
  ADD PRIMARY KEY (`exam_cat_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_answers`
--
ALTER TABLE `model_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_questions`
--
ALTER TABLE `model_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_test_categories`
--
ALTER TABLE `model_test_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `answer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `chapter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `exam_categories`
--
ALTER TABLE `exam_categories`
  MODIFY `exam_cat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `model_answers`
--
ALTER TABLE `model_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `model_questions`
--
ALTER TABLE `model_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `model_test_categories`
--
ALTER TABLE `model_test_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
