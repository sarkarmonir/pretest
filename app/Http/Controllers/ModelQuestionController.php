<?php

namespace App\Http\Controllers;

use App\ModelQuestion;
use App\Model_answer;
use App\ModelTestCategory;
use App\Subject;
use App\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use DB;

class ModelQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['m_question_data'] = ModelQuestion::all();
        $data['model_test_cat_data'] = ModelTestCategory::all();
        $data['subject_data'] = Subject::all();
        $data['chapter_data'] = Chapter::all();
        // dd($data);
        return view('backend.model_question', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->user_type != 2){
            $m_question_data = ModelQuestion::create([
                'model_test_cat_id' => $request->model_test_cat_id,
                'subject_id' => $request->subject_id,
                'chapter_id' => 0,
                'model_question_title' => $request->question_title,
                'question_hint' => $request->hint,
                'previous_exam' => $request->previous_test,
                'created_by' => Auth::user()->id,
                'created_at' => date('Y-m-d'),
            ]);
        }else{
            $m_question_data = ModelQuestion::create([
                'model_test_cat_id' => $request->model_test_cat_id,
                'subject_id' => $request->subject_id,
                'chapter_id' => 0,
                'model_question_title' => $request->question_title,
                'question_hint' => $request->hint,
                'previous_exam' => $request->previous_test,
                'qa_by' => Auth::user()->id,
                'created_at' => date('Y-m-d'),
            ]);
        }
        
        for($i=0; $i< count($request->input('answer')); $i++){
            if($request->correct == $i){
                $correct = 1;
            }else{
                $correct = 0;
            }
            Model_answer::create([ 
                'm_question_id' => $m_question_data->id,
                'answer_option'=> $request->input('answer')[$i],
                'correct_answer'=> $correct,
                'created_at'=> date('Y-m-d'),
            ]);
        }
        Toastr::success('success','Sucessfully Model Test Question Added!!!!');
        return redirect('/model-test');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelQuestion  $modelQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(ModelQuestion $modelQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelQuestion  $modelQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelQuestion $modelQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelQuestion  $modelQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelQuestion $modelQuestion)
    {
        $question_id = $request->ques_id;
        $question_data = ModelQuestion::find($question_id);
        $question_data->model_test_cat_id = $request->model_test_cat_id;
        $question_data->subject_id = $request->subject_id;
        $question_data->chapter_id = 0;
        $question_data->model_question_title = $request->question_title;
        $question_data->question_hint = $request->hint;
        $question_data->previous_exam = $request->previous_test;
        if(Auth::user()->user_type != 2){
            $question_data->created_by = Auth::user()->id;
        }else{
            $question_data->qa_by = Auth::user()->id;
        }
        $question_data->updated_at = date('Y-m-d');
        $question_data->save();
        
        for($i=0; $i< count($request->input('old_answer')); $i++){
            $answer_id = $request->input('old_answer')[$i];
            if($request->correct == $answer_id){
                $correct = 1;
            }else{
                $correct = 0;
            }
            $answer_data = Model_answer::find($answer_id);
            $answer_data->m_question_id = $question_data->id;
            $answer_data->answer_option = $request->input('answer')[$i];
            $answer_data->correct_answer = $correct;
            $answer_data->updated_at = date('Y-m-d');
            $answer_data->save();
        }
        Toastr::success('success','Sucessfully Model Question Updated!!!!');
        return redirect('/model-test');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelQuestion  $modelQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelQuestion $modelQuestion, $id)
    {
        $question_data = ModelQuestion::find($id);
        DB::table('model_answers')->where('m_question_id', $id)->delete();

        $question_data->delete();
        Toastr::success('success','Sucessfully Model Question Deleted!!!!');
        return redirect('/model-test');
    }

    public function get_model_answer_data(Request $request)
    {
        $id = $request->question_id;
        $result = DB::table('model_answers')
        ->where('m_question_id', $id)
        ->get();
        echo json_encode($result);
    }
    public function get_chapter_data(Request $request)
    {
        $id = $request->subject_id;
        $result = DB::table('chapters')
        ->where('subject_id', $id)
        ->get();
        echo json_encode($result);
    }

}
