<?php

namespace App\Http\Controllers;

use App\ExamCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;

class ExamCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['exam_category_data'] = ExamCategory::all();
        return view('backend.exam_category', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new ExamCategory();
        $data->exam_cat_name = $request->name;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }
        if($data->save()){
            Toastr::success('success','Sucessfully Exam Category Added!!!!');
            return redirect('/exam-category');
        }else{
            Toastr::error('opps!!','Data Not Added');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExamCategory  $examCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ExamCategory $examCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExamCategory  $examCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamCategory $examCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExamCategory  $examCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamCategory $examCategory)
    {
        $id = $request->exam_cat_id;
        // echo $id;exit;
        $data = ExamCategory::find($id);

        $data->exam_cat_name = $request->name;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }
        if($data->save()){
            Toastr::success('success','Sucessfully Exam Category Updated!!!!');
            return redirect('/exam-category');
        }else{
            Toastr::error('opps!!','Data Not Updated');
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExamCategory  $examCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamCategory $examCategory, $id)
    {
        $data = ExamCategory::find($id);
        if($data->delete()){
            Toastr::success('success):','Sucessfully Exam Category Deleted!!!!');
            return redirect('/exam-category');
        }else{
            Toastr::error('opps!!','Data Not Deleted');
            return redirect()->back();
        }
    }
}
