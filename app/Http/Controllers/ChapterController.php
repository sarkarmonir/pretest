<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Subject;
use App\ExamCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use DB;
class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['chapter_data'] = Chapter::all();
        $data['subject_data'] = Subject::all();
        $data['exam_cat_data'] = ExamCategory::all();
        return view('backend.chapter', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Chapter();
        $data->chapter_name = $request->name;
        $data->chapter_description = $request->description;
        $data->exam_cat_id = $request->exam_cat_id;
        $data->subject_id = $request->subject_id;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }

        if($data->save()){
            Toastr::success('success','Sucessfully Chapter Added!!!!');
            return redirect('/chapter');
        }else{
            Toastr::error('opps!!','Data Not Added');
            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapter $chapter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chapter $chapter)
    {
        $id = $request->chap_id;
        $data = Chapter::find($id);

        $data->chapter_name = $request->name;
        $data->exam_cat_id = $request->exam_cat_id;
        $data->subject_id = $request->subject_id;
        $data->chapter_description = $request->description;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }

        if($data->save()){
            Toastr::success('success','Sucessfully Chapter Updated!!!!');
            return redirect('/chapter');
        }else{
            Toastr::error('opps!!','Data Not Updated');
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter, $id)
    {
        $data = Chapter::find($id);
        if($data->delete()){
            Toastr::success('success','Sucessfully Chapter Deleted!!!!');
            return redirect('/chapter');
        }else{
            Toastr::error('opps!!','Data Not Deleted');
            return redirect()->back();
        }
    }
    public function get_subject_data(Request $request)
    {
        $id = $request->cat_id;
        $result = DB::table('subjects')
        ->where('exam_cat_id', $id)
        ->get();
        echo json_encode($result);
    }

}
