<?php

namespace App\Http\Controllers;

use App\Subject;
use App\ExamCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['subject_data'] = Subject::all();
        $data['exam_cat_data'] = ExamCategory::all();
        return view('backend.subject', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Subject();
        $data->subject_name = $request->name;
        $data->exam_cat_id = $request->exam_cat_id;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }
        if($data->save()){
            Toastr::success('success','Sucessfully Subjects Added!!!!');
            return redirect('/subject');
        }else{
            Toastr::error('opps!!','Data Not Added');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $id = $request->subject_id;
        $data = Subject::find($id);

        $data->subject_name = $request->name;    
        $data->exam_cat_id = $request->user_type;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }
        if($data->save()){
            Toastr::success('success','Sucessfully Subjects Updated!!!!');
            return redirect('/subject');
        }else{
            Toastr::error('opps!!','Data Not Updated');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject, $id)
    {
        $data = Subject::find($id);

       if($data->delete()){
            Toastr::success('success','Sucessfully Subjects Deleted!!!!');
            return redirect('/subject');
        }else{
            Toastr::error('opps!!','Data Not Deleted');
            return redirect()->back();
        }
    }
}
