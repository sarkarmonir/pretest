<?php

namespace App\Http\Controllers;

use App\Question;
use App\Answer;
use App\Subject;
use App\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use DB;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['question_data'] = Question::all();
        $data['subject_data'] = Subject::all();
        $data['chapter_data'] = Chapter::all();
        // dd($data);
        return view('backend.upload_question', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(Auth::user()->user_type != 2){
            $question_data = Question::create([
                'subject_id' => $request->subject_id,
                'chapter_id' => $request->chapter_id,
                'question_title' => $request->question_title,
                'question_hint' => $request->hint,
                'previous_exam' => $request->previous_test,
                'created_by' => Auth::user()->id,
            ]);
        }else{
            $question_data = Question::create([
                'subject_id' => $request->subject_id,
                'chapter_id' => $request->chapter_id,
                'question_title' => $request->question_title,
                'question_hint' => $request->hint,
                'previous_exam' => $request->previous_test,
                'qa_by' => Auth::user()->id,
            ]);
        }
        
        for($i=0; $i< count($request->input('answer')); $i++){
            if($request->correct == $i){
                $correct = 1;
            }else{
                $correct = 0;
            }
            Answer::create([ 
                'question_id' => $question_data->question_id,
                'answer_option'=> $request->input('answer')[$i],
                'correct_answer'=> $correct,
                'created_at'=> date('Y-m-d'),
            ]);
        }
        Toastr::success('success','Sucessfully Question Added!!!!');
        return redirect('/upload-qa');

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request);
        $question_id = $request->ques_id;
        $question_data = Question::find($question_id);
        $question_data->subject_id = $request->subject_id;
        $question_data->chapter_id = $request->chapter_id;
        $question_data->question_title = $request->question_title;
        $question_data->question_hint = $request->hint;
        $question_data->previous_exam = $request->previous_test;
        if(Auth::user()->user_type != 2){
            $question_data->created_by = Auth::user()->id;
        }else{
            $question_data->qa_by = Auth::user()->id;
        }
        $question_data->save();
        
        for($i=0; $i< count($request->input('old_answer')); $i++){
            $answer_id = $request->input('old_answer')[$i];
            if($request->correct == $answer_id){
                $correct = 1;
            }else{
                $correct = 0;
            }
            $answer_data = Answer::find($answer_id);
            $answer_data->question_id = $question_data->question_id;
            $answer_data->answer_option = $request->input('answer')[$i];
            $answer_data->correct_answer = $correct;
            $answer_data->updated_at = date('Y-m-d');
            $answer_data->save();
        }
        Toastr::success('success','Sucessfully Question Updated!!!!');
        return redirect('/upload-qa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question, $id)
    {
        $question_data = Question::find($id);
        DB::table('answers')->where('question_id', $id)->delete();

        $question_data->delete();
        Toastr::success('success','Sucessfully Question Deleted!!!!');
        return redirect('/upload-qa');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function get_answer_data(Request $request)
    {
        $question_id = $request->question_id;
        $result = DB::table('answers')
        ->where('question_id', $question_id)
        ->get();
        echo json_encode($result);
    }
}
