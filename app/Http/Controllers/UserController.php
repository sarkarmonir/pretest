<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['all_user'] = User::all();
        return view('backend.user.all_user', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $data = new User();
        $data->name =  $request->name;
        $data->email =  $request->email;
        $data->password =  Hash::make($request->password);
        $data->user_type =  $request->type;
        $data->is_active =  1;
        $data->created_by =  Auth::user()->id;

        if($data->save()){
            Toastr::success('success','Sucessfully User Added!!!!');
            return redirect('/user');
        }else{
            Toastr::error('opps!!','Data Not Added');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo json_encode($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id = $request->user_id;
        $data = User::find($user_id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->user_type = $request->user_type;

        if($data->save()){
            Toastr::success('success','Sucessfully User Updated!!!!');
            return redirect('/user');
        }else{
            Toastr::error('opps!!','Data Not Updated');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        if($data->delete()){
            Toastr::success('success):','Sucessfully User Deleted!!!!');
            return redirect('/user');
        }else{
            Toastr::error('opps!!','Data Not Deleted');
            return redirect()->back();
        }
    }
}
