<?php

namespace App\Http\Controllers;

use App\ModelTestCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;

class ModelTestCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['model_test_category_data'] = ModelTestCategory::all();
        return view('backend.model_test_category', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new ModelTestCategory();
        $data->model_test_cat_title = $request->name;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }
        if($data->save()){
            Toastr::success('success','Sucessfully Model Test Category Added!!!!');
            return redirect('/model-test-category');
        }else{
            Toastr::error('opps!!','Data Not Added');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelTestCategory  $modelTestCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ModelTestCategory $modelTestCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelTestCategory  $modelTestCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelTestCategory $modelTestCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelTestCategory  $modelTestCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelTestCategory $modelTestCategory)
    {
        $id = $request->model_test_cat_id;
        $data = ModelTestCategory::find($id);
        $data->model_test_cat_title = $request->name;
        if(Auth::user()->user_type != 2){
            $data->created_by = Auth::user()->id;
        }else{
            $data->qa_by = Auth::user()->id;
        }
        if($data->save()){
            Toastr::success('success','Sucessfully Model Test Category Updated!!!!');
            return redirect('/model-test-category');
        }else{
            Toastr::error('opps!!','Data Not Updated');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelTestCategory  $modelTestCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelTestCategory $modelTestCategory, $id)
    {
        $data = ModelTestCategory::find($id);
        if($data->delete()){
            Toastr::success('success','Sucessfully Model Test Category Deleted!!!!');
            return redirect('/model-test-category');
        }else{
            Toastr::error('opps!!','Data Not Deleted');
            return redirect()->back();
        }
    }
}
