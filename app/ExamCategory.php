<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamCategory extends Model
{
    /**
     * for the mass assignable
     *
     * @var string
     */
    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exam_categories';

    /**
     * The primaryKey of this table with the model.
     *
     * @var string
     */
    protected $primaryKey = 'exam_cat_id';

}
