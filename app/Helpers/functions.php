<?php 

function get_created_by($id){
	$data = DB::table('users')->select('name')->where('id',$id)->first();
	if(isset($data)){
		return $data->name;
	}else{
		return false;
	}
}

function exam_cat_data_by_id($id , $field_name){
	$data = DB::table('exam_categories')->select($field_name)->where('exam_cat_id',$id)->get();
	if(isset($data)){
		return $data;
	}else{
		return false;
	}
}
function model_test_cat_data_by_id($id , $field_name){
	$data = DB::table('model_test_categories')->select($field_name)->where('id',$id)->get();
	if(isset($data)){
		return $data;
	}else{
		return false;
	}
}

function subject_data_by_id($id , $field_name){
	$data = DB::table('subjects')->select($field_name)->where('subject_id',$id)->get();
	if(isset($data)){
		return $data;
	}else{
		return false;
	}
}

function chapter_data_by_id($id , $field_name){
	$data = DB::table('chapters')->select($field_name)->where('chapter_id',$id)->get();
	if(isset($data)){
		return $data;
	}else{
		return false;
	}
}

function get_answer($id){
	$data = DB::table('answers')
				->select('answer_option')->where('question_id',$id)
				->where('correct_answer',1)
				->get();
		if(isset($data)){
			return $data;
		}else{
			return false;
		}
}

function get_model_answer($id){
	$data = DB::table('model_answers')
				->select('answer_option')->where('m_question_id',$id)
				->where('correct_answer',1)
				->get();
		if(isset($data)){
			return $data;
		}else{
			return false;
		}
}