<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelQuestion extends Model
{
    /**
     * for the mass assignable
     *
     * @var string
     */
    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'model_questions';

    /**
     * The primaryKey of this table with the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = false;
}
