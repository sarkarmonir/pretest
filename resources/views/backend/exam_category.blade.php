@extends('backend.layouts.app')
@section('content')
<div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="page-title">Exam Category</h4>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-right page-title-box">
                                <small class="active">Dashboard</small> / 
                                <small class="active">Exam Category</small>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <!-- modal -->
                    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new Exam Category</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/exam-category-store')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Exam Category Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Enter Exam Category Name" required="">
                                        </div>
                                        <button type="submit" class="btn btn-primary waves-effect">Save</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-info waves-effect float-right btn-sm" data-toggle="modal" data-target=".bs-example-modal-center">Add Exam Category</button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="10">ID</th>
                                                <th>Exam Category Name</th>
                                                <th>Created By</th>
                                                <th width="120">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($exam_category_data as $key => $list)
                                            <tr>
                                                <td>{{ $key+1}}</td>
                                                <td>{{ $list->exam_cat_name }}</td>
                                                <td><button class="btn btn-primary btn-sm">{{ strtoupper(get_created_by($list->created_by)) }}</button></td>
                                                <td>
                                                    <!-- <button class="btn btn-info waves-effect btn-sm"><i class="fa fa-user"></i></button> -->
                                                    <button class="btn btn-warning waves-effect btn-sm">
                                                        <a id="item_edit" href="javascript:void(0);"  data-exam_cat_id="<?php echo $list->exam_cat_id; ?>" data-exam_cat_name="<?php echo $list->exam_cat_name; ?>"><i class="fa fa-edit"></i></a>
                                                    </button>
                                                    <button class="btn btn-danger waves-effect btn-sm" onclick="return deleteCertification(<?php echo $list->exam_cat_id; ?>)"><i class="fa fa-trash"></i></button>
                                                    <form id="delete-form-{{$list->exam_cat_id}}" action="{{url('/exam-category-delete/'.$list->exam_cat_id)}}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    
                </div>
                <!-- container-fluid -->
                <!-- modal -->
                    <div id="modal-edit" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create Exam Category</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/exam-cat-update')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Exam Category Name</label>
                                            <input id="u_name" type="text" class="form-control" name="name" placeholder="Enter Exam Category Name">
                                        </div>
                                        <input id="u_id" type="hidden" name="exam_cat_id" value="">
                                        <button type="submit" class="btn btn-primary waves-effect">Update</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('tr td button').on('click','#item_edit',function(){
            var id = $(this).data('exam_cat_id');
            var name = $(this).data('exam_cat_name');
            $('#modal-edit').modal('show');
            $('#u_id').val(id);
            $('#u_name').val(name);
    });
    })
</script>
<script type="text/javascript">
            function deleteCertification(id){
                const swalWithBootstrapButtons = Swal.mixin({
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  reverseButtons: true
                }).then((result) => {
                  if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                  } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                    swalWithBootstrapButtons(
                      'Cancelled',
                      'Your Data is safe :)',
                      'error'
                    )
                  }
                })
            }
        </script>
@endsection