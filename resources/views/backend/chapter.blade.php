@extends('backend.layouts.app')
@section('css')

@endsection
@section('content')
<div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="page-title">Chapters</h4>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-right page-title-box">
                                <small class="active">Dashboard</small> / 
                                <small class="active">Chapters</small>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <!-- modal -->
                    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new Chapters</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/chapter-store')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Chapters Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Enter Full Name" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Exam category Name</label>
                                            <select class="form-control" name="exam_cat_id" required="" id="exam_cat_id" onchange="return getSubjectData();">
                                                <option value="">Select Exam Category Name</option>
                                                @foreach($exam_cat_data as $exam_cat_list)
                                                <option value="{{ $exam_cat_list->exam_cat_id }}">{{ $exam_cat_list->exam_cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputEmail1">Subject Name</label>
                                            <select class="form-control" name="subject_id" id="subject_data" required="">
                                                <option value="">Select Subject Name</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Chapters Description</label>
                                            <textarea class="form-control" id="myTeaxtarea" name="description" placeholder="Enter Chapter Description" required=""></textarea>

                                        </div>
                                        <button type="submit" class="btn btn-primary waves-effect">Save</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-info waves-effect float-right btn-sm" data-toggle="modal" data-target=".bs-example-modal-center">Add Chapters</button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="10">ID</th>
                                                <th>Chapter Name</th>
                                                <th>Subject Name</th>
                                                <th>Exam Cat Name</th>
                                                <th>Created By</th>
                                                <th>Description</th>
                                                <th width="120">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($chapter_data as $key => $list)
                                            <tr>
                                                <td>{{ $key+1}}</td>
                                                <td>{{ $list->chapter_name }}</td>
                                                <td><?php echo subject_data_by_id($list->subject_id, 'subject_name')[0]->subject_name; ?></td>
                                                <td><?php echo exam_cat_data_by_id($list->exam_cat_id, 'exam_cat_name')[0]->exam_cat_name; ?></td>
                                                 <td><button class="btn btn-primary btn-sm">{{ strtoupper(get_created_by($list->created_by)) }}</button></td>
                                                <td>{{ str_limit($list->chapter_description,10)}}</td>
                                                <td>
                                                    <!-- <button class="btn btn-info waves-effect btn-sm"><i class="fa fa-user"></i></button> -->
                                                    <button class="btn btn-warning waves-effect btn-sm">
                                                        <a id="item_edit" href="javascript:void(0);" data-chapter_id="<?php echo $list->chapter_id; ?>" data-subject_id="<?php echo $list->subject_id; ?>" data-exam_cat_id="<?php echo $list->exam_cat_id; ?>" data-chapter_name="<?php echo $list->chapter_name; ?>"  data-chapter_description="<?php echo $list->chapter_description; ?>"><i class="fa fa-edit"></i></a>
                                                    </button>
                                                    <button class="btn btn-danger waves-effect btn-sm" onclick="return deleteCertification(<?php echo $list->chapter_id; ?>)"><i class="fa fa-trash"></i></button>
                                                    <form id="delete-form-{{$list->chapter_id}}" action="{{url('/chapter-delete/'.$list->chapter_id)}}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    
                </div>
                <!-- container-fluid -->
                <!-- modal -->
                    <div id="modal-edit" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/chapter-update')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Chapter Name</label>
                                            <input id="chap_name" type="text" class="form-control" name="name" placeholder="Enter Full Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Exam Category Name</label>
                                            <select id="edit_exam_cat_id" class="form-control" name="exam_cat_id" required="" onchange="return editGetSubjectData();">
                                                <option value="">Select Exam Category</option>
                                                @foreach($exam_cat_data as $exam_cat_list)
                                                <option value="{{ $exam_cat_list->exam_cat_id }}">{{ $exam_cat_list->exam_cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Subject Name</label>
                                            <select id="edit_subject_data" class="form-control" name="subject_id">
                                                <option value="">Select Subject</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Chapter Description</label>
                                            <textarea id="chap_description" class="form-control" name="description" placeholder="Enter Chapter Description"></textarea>
                                        </div>
                                        <input id="chap_id" type="hidden" name="chap_id" value="">
                                        <button type="submit" class="btn btn-primary waves-effect">Update</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
@endsection
@section('js')
<!-- <script src="https://cloud.tinymce.com/5/tinymce.min.js"></script> -->
<script type="text/javascript">
    $(document).ready(function(){
        // tinymce.init({
        //     selector:'#myTeaxtarea'
        // });
        $('tr td button').on('click','#item_edit',function(){
            var chapter_id = $(this).data('chapter_id');
            var subject_id = $(this).data('subject_id');
            var exam_cat_id = $(this).data('exam_cat_id');
            var chapter_name = $(this).data('chapter_name');
            var chapter_description = $(this).data('chapter_description');
            $('#modal-edit').modal('show');
            $('#chap_id').val(chapter_id);
            $('#edit_exam_cat_id').val(exam_cat_id);
            // $('#subject_id').val(subject_id);
            $('#chap_name').val(chapter_name);
            $('#chap_description').val(chapter_description);
            // alert(exam_cat_id);
            $.ajax({
                url: BASE_URL+"/get-subject-data-by-id",
                type: 'GET',
                data:{
                  'cat_id': exam_cat_id,
                },
                success: function(data){
                    $('#edit_subject_data').empty();
                    var html = '';
                    $.each(JSON.parse(data), function (key, val) {
                        console.log(val);
                        html += '<option value="'+val.subject_id+'">'+val.subject_name+'</option>';
                    });
                    $('#edit_subject_data').append(html);
                    $('#edit_subject_data').val(subject_id);
                }
          });
    });
    })
</script>
<script type="text/javascript">
    // subject onchange chapter data get with ajax start
    function getSubjectData(){
        var exam_cat_id = $('#exam_cat_id').val();
        $.ajax({
                url: BASE_URL+"/get-subject-data-by-id",
                type: 'GET',
                data:{
                  'cat_id': exam_cat_id,
                },
                success: function(data){
                    $('#subject_data').empty();
                    var html = '';
                    $.each(JSON.parse(data), function (key, val) {
                        console.log(val);
                        html += '<option value="'+val.subject_id+'">'+val.subject_name+'</option>';
                    });
                    $('#subject_data').append(html);

                }
          });// subject onchange chapter data get with ajax End
    }

    function editGetSubjectData(){
        var exam_cat_id = $('#edit_exam_cat_id').val();
        $.ajax({
                url: BASE_URL+"/get-subject-data-by-id",
                type: 'GET',
                data:{
                  'cat_id': exam_cat_id,
                },
                success: function(data){
                    $('#edit_subject_data').empty();
                    var html = '';
                    $.each(JSON.parse(data), function (key, val) {
                        console.log(val);
                        html += '<option value="'+val.subject_id+'">'+val.subject_name+'</option>';
                    });
                    $('#edit_subject_data').append(html);

                }
          });// subject onchange chapter data get with ajax End
    }
</script>
<script type="text/javascript">
            function deleteCertification(id){
                const swalWithBootstrapButtons = Swal.mixin({
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  reverseButtons: true
                }).then((result) => {
                  if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                  } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                    swalWithBootstrapButtons(
                      'Cancelled',
                      'Your Data is safe :)',
                      'error'
                    )
                  }
                })
            }
        </script>
@endsection