@extends('backend.layouts.app')
@section('content')
<div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="page-title">Model Questions</h4>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-right page-title-box">
                                <small class="active">Dashboard</small> / 
                                <small class="active">Model Questions</small>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <!-- modal -->
                    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new Model Test Questions</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/model-question-store')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Model Test Category Title</label>
                                            <select class="form-control" name="model_test_cat_id" required="">
                                                <option value="">Select Model Test Category</option>
                                                @if($model_test_cat_data)
                                                @foreach($model_test_cat_data as $model_test_cat_list)
                                                <option value="{{ $model_test_cat_list->id }}">{{ $model_test_cat_list->model_test_cat_title }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Subject</label>
                                            <select class="form-control" id="subject_id" name="subject_id" required="">
                                                <option value="">Select Subject</option>
                                                @if($subject_data)
                                                @foreach($subject_data as $subject_list)
                                                <option value="{{ $subject_list->subject_id }}">{{ $subject_list->subject_name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="exampleInputEmail1">Chapter</label>
                                            <select class="form-control" name="chapter_id" required="" id="chapter_data">
                                                <option value="">Select Chapter</option>
                                            </select>
                                        </div> -->
                                         <div class="form-group">
                                            <label for="exampleInputEmail1">Questions?</label>
                                            <input type="text" class="form-control" name="question_title" placeholder="Enter Questions?" required="">
                                        </div>
                                        @for($question=0; $question<=3; $question++)
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <input type="radio" name="correct" aria-label="Checkbox for following text input" value="{{$question}}">
                                                </div>
                                            </div>
                                            <input type="text" class="form-control" name="answer[]" aria-label="Text input with checkbox" placeholder="Option {{$question}}" required="">
                                        </div>
                                        @endfor
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Question Hint Test</label>
                                            <input type="text" class="form-control" name="hint" value="" placeholder="Enter Previous Test Exam & Year">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Previous Test</label>
                                            <input type="text" class="form-control" name="previous_test" value="" placeholder="Enter Previous Test Exam & Year">
                                        </div>
                                        <button type="submit" class="btn btn-primary waves-effect">Save</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-info waves-effect float-right btn-sm" data-toggle="modal" data-target=".bs-example-modal-center">Add Model Test</button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="10">ID</th>
                                                <th>Question Title</th>
                                                <th>Answer</th>
                                                <th>Model Test Category</th>
                                                <th>Subject</th>
                                                <th>Created By</th>
                                                <th width="120">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($m_question_data)
                                            @foreach($m_question_data as $key => $list)
                                            <tr>
                                                <td>{{ $key+1}}</td>
                                                <td>{{ $list->model_question_title }}</td>
                                                <td><?php echo get_model_answer($list->id)[0]->answer_option ?></td>
                                                <td><?php echo model_test_cat_data_by_id($list->model_test_cat_id, 'model_test_cat_title')[0]->model_test_cat_title; ?></td>
                                                <td><?php echo subject_data_by_id($list->subject_id, 'subject_name')[0]->subject_name; ?></td>
                                                <td><button class="btn btn-primary btn-sm">{{ strtoupper(get_created_by($list->created_by)) }}</button></td>
                                                <td>
                                                    <!-- <button class="btn btn-info waves-effect btn-sm"><i class="fa fa-user"></i></button> -->
                                                    <button class="btn btn-warning waves-effect btn-sm">
                                                        <a id="item_edit" href="javascript:void(0);" data-question_id="<?php echo $list->id; ?>" data-model_test_cat_id="<?php echo $list->model_test_cat_id; ?>" data-chapter_id="<?php echo $list->chapter_id; ?>" data-subject_id="<?php echo $list->subject_id; ?>" data-question_title="<?php echo $list->model_question_title; ?>" data-qa_previous_test="<?php echo $list->previous_exam; ?>" data-question_hint="<?php echo $list->question_hint; ?>"><i class="fa fa-edit"></i></a>
                                                    </button>
                                                    <button class="btn btn-danger waves-effect btn-sm" onclick="return deleteCertification(<?php echo $list->id; ?>)"><i class="fa fa-trash"></i></button>
                                                    <form id="delete-form-{{$list->id}}" action="{{url('/model-question-delete/'.$list->id)}}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    
                </div>
                <!-- container-fluid -->
                <!-- modal -->
                    <div id="modal-edit" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Edit a Question</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/model-question-update')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Model Test Category Title</label>
                                            <select class="form-control" id="model_test_cat_id" name="model_test_cat_id" required="">
                                                <option value="">Select Model Test Category</option>
                                                @if($model_test_cat_data)
                                                @foreach($model_test_cat_data as $model_test_cat_list)
                                                <option value="{{ $model_test_cat_list->id }}">{{ $model_test_cat_list->model_test_cat_title }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Subject</label>
                                            <select class="form-control" id="sub_id" name="subject_id" required="" onchange="return editGetchapterData();">
                                                <option value="">Select Subject</option>
                                                @foreach($subject_data as $subject_list)
                                                <option value="{{ $subject_list->subject_id }}">{{ $subject_list->subject_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputEmail1">Questions?</label>
                                            <input type="text" id="q_title" class="form-control" name="question_title" placeholder="Enter Questions?" required="">
                                        </div>
                                        <div  id="answer_data">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Question Hint Test</label>
                                            <input id="question_hint" type="text" class="form-control" name="hint" value="" placeholder="Enter Previous Test Exam & Year">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Previous Test</label>
                                            <input id="previous_test" type="text" class="form-control" name="previous_test" value="" placeholder="Enter Previous Test Exam & Year" required="">
                                        </div>
                                        <input id="ques_id" type="hidden" name="ques_id" value="">
                                        <button type="submit" class="btn btn-primary waves-effect">Update</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->


@endsection
@section('js')
<!-- model question edit with ajax call start -->
<script type="text/javascript">
    $(document).ready(function(){
        $('tr td button').on('click','#item_edit',function(){
            var question_id = $(this).data('question_id');
            var model_test_cat_id = $(this).data('model_test_cat_id');
            var chapter_id = $(this).data('chapter_id');
            var subject_id = $(this).data('subject_id');
            var question_title = $(this).data('question_title');
            var question_hint = $(this).data('question_hint');
            var qa_previous_test = $(this).data('qa_previous_test');
            $('#modal-edit').modal('show');
            $('#ques_id').val(question_id);
            $('#model_test_cat_id').val(model_test_cat_id);
            $('#sub_id').val(subject_id);
            // $('#chapter_id').val(chapter_id);
            $('#q_title').val(question_title);
            $('#question_hint').val(question_hint);
            $('#previous_test').val(qa_previous_test);
          //   $.ajax({
          //       url: BASE_URL+"/get-chapter-data-by-id",
          //       type: 'GET',
          //       data:{
          //         'subject_id': subject_id,
          //       },
          //       success: function(data){
          //           $('#edit_chapter_data').empty();
          //           var html = '';
          //           $.each(JSON.parse(data), function (key, val) {
          //               console.log(val);
          //               html += '<option value="'+val.chapter_id+'">'+val.chapter_name+'</option>';
          //           });
          //           $('#edit_chapter_data').append(html);
          //           $('#edit_chapter_data').val(chapter_id);
          //       }
          //    });
            $.ajax({
                url: BASE_URL+"/get-model-question-answer-by-id",
                type: 'GET',
                data:{
                  'question_id': question_id,
                },
                success: function(data){
                    $('#answer_data').empty();
                    var html = '';
                    $.each(JSON.parse(data), function (key, val) {
                        var incremeent = key;
                        if(val.correct_answer === 1){
                            var correct_answer = 'checked="true"';
                        }else{
                            var correct_answer = '';
                        }                        
                        html += '<div class="input-group mb-3">'+
                                '<div class="input-group-prepend">'+
                                    '<div class="input-group-text">'+
                                        '<input type="radio" name="correct" aria-label="Checkbox for following text input" value="'+val.id+'" id="cr_answer_'+incremeent+'" '+ correct_answer +'>'+
                                    '</div>'+
                                '</div>'+
                                '<input type="hidden" class="form-control" name="old_answer[]" value="'+val.id+'">'+
                                '<input type="text" class="form-control" name="answer[]" value="'+val.answer_option+'">'+
                                '</div>';
                    });
                    $('#answer_data').append(html);

                    
                }
          });// model question edit with ajax call start 


        });
    })
</script>

<script type="text/javascript">
    // subject onchange chapter data get with ajax start
    function getChapterData(){
        var subject_id = $('#subject_id').val();
        $.ajax({
                url: BASE_URL+"/get-chapter-data-by-id",
                type: 'GET',
                data:{
                  'subject_id': subject_id,
                },
                success: function(data){
                    $('#chapter_data').empty();
                    var html = '';
                    $.each(JSON.parse(data), function (key, val) {
                        html += '<option value="'+val.chapter_id+'">'+val.chapter_name+'</option>';
                    });
                    $('#chapter_data').append(html);

                    
                }
          });// subject onchange chapter data get with ajax End
    }

    function editGetchapterData(){
        var subject_id = $('#sub_id').val();
        $.ajax({
                url: BASE_URL+"/get-chapter-data-by-id",
                type: 'GET',
                data:{
                  'subject_id': subject_id,
                },
                success: function(data){
                    $('#edit_chapter_data').empty();
                    var html = '';
                    $.each(JSON.parse(data), function (key, val) {
                        console.log(val);
                        html += '<option value="'+val.chapter_id+'">'+val.chapter_name+'</option>';
                    });
                    $('#edit_chapter_data').append(html);
                }
        });// subject onchange chapter data get with ajax End
    }
</script>
<script type="text/javascript">
            function deleteCertification(id){
                const swalWithBootstrapButtons = Swal.mixin({
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  reverseButtons: true
                }).then((result) => {
                  if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                  } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                    swalWithBootstrapButtons(
                      'Cancelled',
                      'Your Data is safe :)',
                      'error'
                    )
                  }
                })
            }
        </script>
@endsection