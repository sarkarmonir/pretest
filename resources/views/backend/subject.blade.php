

@extends('backend.layouts.app')
@section('content')
<div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="page-title">Subjects</h4>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-right page-title-box">
                                <small class="active">Dashboard</small> / 
                                <small class="active">Subjects</small>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <!-- modal -->
                    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new Subjects</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/subject-store')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Subjects Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Enter Full Name" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Exam category Name</label>
                                            <select class="form-control" name="exam_cat_id" required="">
                                                <option value="">Select Exam Category</option>
                                                @foreach($exam_cat_data as $exam_cat_list)
                                                <option value="{{ $exam_cat_list->exam_cat_id }}">{{ $exam_cat_list->exam_cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary waves-effect">Save</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-info waves-effect float-right btn-sm" data-toggle="modal" data-target=".bs-example-modal-center">Add Subject</button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="10">ID</th>
                                                <th>Subject Name</th>
                                                <th>Exam Cat Name</th>
                                                <th>Created Bys</th>
                                                <th width="120">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($subject_data as $key => $list)
                                            <tr>
                                                <td>{{ $key+1}}</td>
                                                <td>{{ $list->subject_name }}</td>
                                                <td><?php echo exam_cat_data_by_id($list->exam_cat_id, 'exam_cat_name')[0]->exam_cat_name; ?></td>
                                                 <td><button class="btn btn-primary btn-sm">{{ strtoupper(get_created_by($list->created_by)) }}</button></td>
                                                <td>
                                                    <!-- <button class="btn btn-info waves-effect btn-sm"><i class="fa fa-user"></i></button> -->
                                                    <button class="btn btn-warning waves-effect btn-sm">
                                                        <a id="item_edit" href="javascript:void(0);"  data-sub_id="<?php echo $list->subject_id; ?>" data-name="<?php echo $list->subject_name; ?>"  data-exam_cat_id="<?php echo $list->exam_cat_id; ?>"><i class="fa fa-edit"></i></a>
                                                    </button>
                                                    <button class="btn btn-danger waves-effect btn-sm" onclick="return deleteCertification(<?php echo $list->subject_id; ?>)"><i class="fa fa-trash"></i></button>
                                                    <form id="delete-form-{{$list->subject_id}}" action="{{url('/subject-delete/'.$list->subject_id)}}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    
                </div>
                <!-- container-fluid -->
                <!-- modal -->
                    <div id="modal-edit" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/subject-update')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Subject Name</label>
                                            <input id="u_name" type="text" class="form-control" name="name" placeholder="Enter Full Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Exam Category Name</label>
                                            <select id="u_type" class="form-control" name="user_type">
                                                <option value="">Select Exam Category</option>
                                                @foreach($exam_cat_data as $exam_cat_list)
                                                <option value="{{ $exam_cat_list->exam_cat_id }}">{{ $exam_cat_list->exam_cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input id="u_id" type="hidden" name="subject_id" value="">
                                        <button type="submit" class="btn btn-primary waves-effect">Update</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('tr td button').on('click','#item_edit',function(){
            var id = $(this).data('sub_id');
            var name = $(this).data('name');
            var exam_cat_id = $(this).data('exam_cat_id');
            $('#modal-edit').modal('show');
            $('#u_id').val(id);
            $('#u_name').val(name);
            $('#u_type').val(exam_cat_id);
    });
    })
</script>
<script type="text/javascript">
            function deleteCertification(id){
                const swalWithBootstrapButtons = Swal.mixin({
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  reverseButtons: true
                }).then((result) => {
                  if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                  } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                    swalWithBootstrapButtons(
                      'Cancelled',
                      'Your Data is safe :)',
                      'error'
                    )
                  }
                })
            }
        </script>
@endsection