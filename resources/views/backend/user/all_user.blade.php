@extends('backend.layouts.app')
@section('content')
<div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="page-title-box">
                                <h4 class="page-title">Users</h4>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-right page-title-box">
                                <small class="active">Dashboard</small> / 
                                <small class="active">Users</small>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <!-- modal -->
                    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/user-store')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Enter Full Name" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="email" class="form-control" name="email" placeholder="Enter Email Address" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Password</label>
                                            <input type="password" class="form-control" name="password" placeholder="Enter Password" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">User role</label>
                                            <select class="form-control" name="type" required="">
                                                <option value="">Select Your Type</option>
                                                <option value="1">Question uploader</option>
                                                <option value="2">QA Tester</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary waves-effect">Save</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-info waves-effect float-right btn-sm" data-toggle="modal" data-target=".bs-example-modal-center">Add User</button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="10">ID</th>
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>User Type</th>
                                                <th>Status</th>
                                                <th width="120">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($all_user as $key => $list)
                                            <tr>
                                                <td>{{ $key+1}}</td>
                                                <td>{{ $list->name }}</td>
                                                <td>{{ $list->email }}</td>
                                                <td>
                                                    <?php 
                                                        if($list->user_type == 0){
                                                            echo "Super Admin";
                                                        }
                                                        else if ($list->user_type == 1) {
                                                            echo "Question Uploader";
                                                        }
                                                        else if ($list->user_type == 2) {
                                                            echo "QA Tester";
                                                        }
                                                        else if ($list->user_type == 3) {
                                                            echo "General User";
                                                        }
                                                        else{
                                                            echo "User Type Not Defined";
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if($list->is_active === 0){
                                                            echo "InActive";
                                                        }else{
                                                            echo "Active";
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                   <!--  <button class="btn btn-info waves-effect btn-sm"><i class="fa fa-user"></i></button> -->
                                                    <button class="btn btn-warning waves-effect btn-sm">
                                                        <a id="item_edit" href="javascript:void(0);"  data-user_id="<?php echo $list->id; ?>" data-user_name="<?php echo $list->name; ?>"  data-user_email="<?php echo $list->email; ?>"  data-user_type="<?php echo $list->user_type; ?>"><i class="fa fa-edit"></i></a>
                                                    </button>
                                                    <button class="btn btn-danger waves-effect btn-sm" onclick="return deleteCertification(<?php echo $list->id; ?>)"><i class="fa fa-trash"></i></button>
                                                    <form id="delete-form-{{$list->id}}" action="{{url('/user-delete/'.$list->id)}}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    
                </div>
                <!-- container-fluid -->
                <!-- modal -->
                    <div id="modal-edit" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0">Create a new User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('/user-update')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input id="u_name" type="text" class="form-control" name="name" placeholder="Enter Full Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input id="u_email" type="email" class="form-control" name="email" placeholder="Enter Email Address">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">User role</label>
                                            <select id="u_type" class="form-control" name="user_type">
                                                <option value="">Select Your Type</option>
                                                <option value="1">Question uploader</option>
                                                <option value="2">QA Tester</option>
                                            </select>
                                        </div>
                                        <input id="u_id" type="hidden" name="user_id" value="">
                                        <button type="submit" class="btn btn-primary waves-effect">Save</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('tr td button').on('click','#item_edit',function(){
            var id = $(this).data('user_id');
            var name = $(this).data('user_name');
            var email = $(this).data('user_email');
            var user_type = $(this).data('user_type');
            $('#modal-edit').modal('show');
            $('#u_id').val(id);
            $('#u_name').val(name);
            $('#u_email').val(email);
            $('#u_type').val(user_type);
    });
    })
</script>
<script type="text/javascript">
            function deleteCertification(id){
                const swalWithBootstrapButtons = Swal.mixin({
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  reverseButtons: true
                }).then((result) => {
                  if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                  } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                  ) {
                    swalWithBootstrapButtons(
                      'Cancelled',
                      'Your Data is safe :)',
                      'error'
                    )
                  }
                })
            }
        </script>
@endsection